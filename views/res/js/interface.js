/**
 * @description Main script for the Login module.
 * @author Ethan Crist
 **/

// Private functions for the Login module.
// Note: it's wrapped in a function so as to not interfere with the website's current variables.
(() => {
    const Plugin = {
        loginURL: '[[= model.loginURL ]]',

        currentTab: Core.getParameter('tab'),

        implementationURL: location.pathname,

        getFirstPath: function() {
            /**
            * @return The first path in the URL, i.e. "dog" in "https://mysite.com/dog/cat"
            **/
            const base = location.href.split('/')[3]
            // Account for ? and # in parameters
            const endOfBase = base.indexOf('?') > -1 ? base.indexOf('?') : (base.indexOf('#') > -1 ? base.indexOf('#') : base.length)
            return base.substring(0, endOfBase) 
        },

        start: function() {
            /**
             * @purpose Run the initial logic for the Login module.
             **/
            if ('/'+Plugin.getFirstPath().toLowerCase() === Plugin.loginURL) Login.open()

            Plugin.listen() 
        },

        listen: function() {
            /**
             * @purpose Run the initial logic for the Login module.
             **/

            // Removing potential duplicate listeners.
            $('#__narrownode-login-plugin-close, #__narrownode-login-plugin-background').unbind()
            $('#__narrownode-login-plugin-submit').unbind()
            $('#__narrownode-login-plugin-register').unbind()
            $('#__narrownode-login-plugin-input').unbind()
            $('[id^="__narrownode-login-plugin-tab-"]').unbind()

            // Adding listeners for the Login module.
            $('#__narrownode-login-plugin-close, #__narrownode-login-plugin-background').click(Login.exit)
            $('#__narrownode-login-plugin-submit').click(Plugin.signIn)
            $('#__narrownode-login-plugin-register').click(Plugin.register)
            $('#__narrownode-login-plugin input').keypress(function(e) {
                // If the user presses enter, submit.
                if (e.keyCode === 13) {
                    switch (Plugin.currentTab) {
                        case "login":
                            Plugin.signIn()
                            break;
                        case "register":
                            Plugin.register()
                            break;
                        case "forgot":
                            break;
                    }
                }
            })
            $('[id^="__narrownode-login-plugin-tab-"]').click((e) => {
                // Jumping to appropriate tab based on which tab link was clicked.
                const tab = e.target.id.split('tab-')[1]
                Login.jumpToTab(tab)
            })
        },

        isEmailAddress: function(input) {
            /**
             * @purpose Check if a string is an email address.
             **/
            return /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/.test(input)
        },

        responseMessage: function(message, status) {
            /**
             * @purpose Sign in with the given username and password.
             **/

            // Falling back on a status of 'DEFAULT'.
            status = status || 'DEFAULT'

            // Removing previous styling from response area...
            $('#__narrownode-login-plugin-response').removeClass()

            // status options: SUCCESS, INFO, WARNING, ERROR
            // Adding new styling from response area...
            $('#__narrownode-login-plugin-response').addClass('__narrownode-login-plugin-response-'+status)

            // Filling the response message into the response area...
            $('#__narrownode-login-plugin-response').html(message).show()

            // Showing the Login module if it isn't already being shown. 
            Login.open()
        },

        allowSubmission: function(enable) {
            /**
             * @purpose Toggle enabling of submission for all forms.
             **/

            // Dis/enabling clicking of login button.
            $('#__narrownode-login-plugin-submit')[enable ? 'removeAttr' : 'attr']('disabled', true)

            // Dis/enabling clicking of register button.
            $('#__narrownode-login-plugin-register')[enable ? 'removeAttr' : 'attr']('disabled', true)

            // Dis/enabling pressing enter to submit the form.
            // Note: To re-enable this, all the event listeners for the plugin are merely reset to re-listen for pressing enter.
            //       This does account for and handle duplicates by unbinding pre-exiting listeners first.
            enable ? Plugin.listen() : $('#__narrownode-login-plugin input').unbind()
        },

        signIn: function() {
            /**
             * @purpose Sign in with the given username and password.
             **/
            const username = $('#__narrownode-login-plugin-username').val()
            const password = $('#__narrownode-login-plugin-password').val()

            // Making sure the username and password aren't empty...
            if (username === "" || password === "") return Plugin.responseMessage('Please fill out all fields.', 'ERROR')

            // Failsafe to not proceed if the login button is already disabled.
            if ($('#__narrownode-login-plugin-submit').is(':disabled')) return

            // Making a login request to the server.
            Plugin.responseMessage('Connecting...')

            // Disabling submission while connecting...
            Plugin.allowSubmission(false)

            $.post(Plugin.loginURL, { login: { username: username, password: password }, loginURL: Plugin.loginURL }, (res) => {
                Plugin.responseMessage(res.message, res.status)

                // Re-enabling submission.
                Plugin.allowSubmission(true)

                if (res.status === 'SUCCESS' && typeof Login.onLogin === 'function') Login.onLogin()
            })
        },

        register: function() {
            /**
             * @purpose Create a new account with the given fields.
             **/
            const email = $('#__narrownode-login-plugin-new-email').val()
            const username = $('#__narrownode-login-plugin-new-username').val()
            const password = $('#__narrownode-login-plugin-new-password').val()
            const confirmPassword = $('#__narrownode-login-plugin-confirm-password').val()
            const captcha = $('#__narrownode-login-plugin-captcha').val()

            // Making sure the username and password aren't empty...
            if (email === "" || username === "" || password === "") return Plugin.responseMessage('Please fill out all fields.', 'ERROR')

            // Making sure the email is an email and the username doesn't include ' ' or '/'.
            if (!Plugin.isEmailAddress(email)) return Plugin.responseMessage('Invalid email.', 'ERROR')
            if (username.match(/ |\//g)) return Plugin.responseMessage('Invalid username.', 'ERROR')

            // Making sure passwords match...
            if (password !== confirmPassword) return Plugin.responseMessage('Your passwords do not match.', 'WARNING')

            // Failsafe to not proceed if the register button is already disabled.
            if ($('#__narrownode-login-plugin-register').is(':disabled')) return

            // Making a login request to the server.
            Plugin.responseMessage('Connecting...')

            // Disabling submission while connecting...
            Plugin.allowSubmission(false)

            $.post(Plugin.loginURL, { register: { email: email, username: username, password: password, captcha: captcha }, loginURL: Plugin.loginURL }, (res) => {
                Plugin.responseMessage(res.message, res.status)

                // Re-enabling submission.
                Plugin.allowSubmission(true)

                // If the user was successfully registered, run some logic.
                if (res.status === 'SUCCESS' || res.status === 'INFO') {
                    if (typeof Login.onRegister === 'function') Login.onRegister()

                    // Jumping to login tab in case the user wants to log in immediately.
                    Login.jumpToTab('login')

                    // Autofilling the username field and jumping to password field for them.
                    $('#__narrownode-login-plugin-username').val(username)
                    $('#__narrownode-login-plugin-password').focus()
                }
            })
        },

        toggleDisplay: function(toggle) {
            /**
             * @purpose Toggle the display of the module.
             **/
            $('#__narrownode-login-plugin').css({ 'display': toggle ? 'flex' : 'none' })
        },

        toggleLoginURL: function(toggle) {
            /**
             * @purpose Change around the browser URL and open/close the login box accordingly.
             **/

            // No need to update the history in this case; the user landed on the page already at the login URL.
            if (Plugin.loginURL === Plugin.implementationURL) return

            history.pushState(null, "Login", toggle ? Plugin.loginURL : Plugin.implementationURL)
        },

        runExitCode: function() {
            /**
             * @purpose Run the base code to exit the Login module.
             **/

            // Not closing the Login module if it's already closed.
            if (!Login.isOpen()) return

            Plugin.toggleDisplay(false)
            Plugin.toggleLoginURL(false)

            // Running implementation callback if it was set.
            if (typeof Login.onClose === 'function') Login.onClose()
        },

        focusFirstInput: function() {
            /**
             * @purpose Focus the first input that is not filled and visible on the Login module.
             **/
            const inputs = $('#__narrownode-login-plugin input:visible')

            inputs.each((i) => {
                // If this input is blank, or if it's the last one in the loop, focus it and quit looping through the rest of the inputs.
                if (inputs[i].value === "" || (i+1) === inputs.length) {
                    inputs[i].focus()
                    return false
                }
            })
        }
    }

// Public functions for the Login module.
// These functions must be within the wrapper in order to access Plugin's variables, but
// they will still be public regardless because Login was instantiated outside the wrapper.
Login.isOpen = function() {
    /**
     * @purpose Check if the Login module is already open.
     **/
    return $('#__narrownode-login-plugin').is(':visible')
}
Login.open = function() {
    /**
     * @purpose Open the Login module.
     **/
    
    // Not opening the Login module if it's already open.
    if (Login.isOpen()) return

    // Jumping to a tab if it was specified as a URL parameter.
    // narrownode.org/login?tab=register => Login.jumpToTab('register') \\ etc

    Plugin.toggleDisplay(true)
    Plugin.toggleLoginURL(true)
    Login.jumpToTab(Plugin.currentTab)
}
Login.exit = function(delay) {
    /**
     * @purpose Close the Login module.
     **/

    // Setting a default delay of 0ms.
    delay = delay || 0

    // No delay; running exit code immediately.
    if (delay < 1) Plugin.runExitCode() 

    // Running exit code with set delay.
    setTimeout(Plugin.runExitCode, delay)
}
Login.jumpToTab = function(tab) {
    /**
     * @purpose Jump to the Sign in, Register, or Forgot Password tab.
     **/

    // Falling back to Sign in tab if tab was not found in registry of tabs.
    const tabs = [ 'login', 'register', 'forgot' ]
    if (!tab) tab = 'login'
    Plugin.currentTab = tab = tabs.indexOf(tab.toLowerCase()) < 0 ? 'login' : tab.toLowerCase()

    if (Plugin.currentTab) Core.setParameter('tab', Plugin.currentTab)

    // Removing all tab-specific classes from the "content" div.
    tabs.forEach((val, i) => {
        // Remove the class if it starts with "__narrownode-login-plugin-tab-"
        $('#__narrownode-login-plugin-content').removeClass('__narrownode-login-plugin-tab-'+val)
    })

    // Adding the active tab class to the content div.
    $('#__narrownode-login-plugin-content').addClass('__narrownode-login-plugin-tab-'+tab)
    // Hiding all tab-specific elements.
    $('[class^="__narrownode-login-plugin-tab-"]').hide()

    // "Deactivating" all tab-specific elements.
    $('[class^="__narrownode-login-plugin-tab-"]').removeClass('__narrownode-login-plugin-active')

    // Showing the tab-specific elements for the new active tab.
    // Also ensuring the title tabs themselves stay shown. (Marked by IDs instead of classes.)
    $('.__narrownode-login-plugin-tab-'+tab).show()
    $('[id^="__narrownode-login-plugin-tab-"]').show()

    // "Activating" the new active tab.
    $('.__narrownode-login-plugin-tab-'+tab).addClass('__narrownode-login-plugin-active')

    // Focusing the first empty input of the open tab.
    Plugin.focusFirstInput() 
}


/**
 * @purpose Pipe the responseMessage function to a public function called "sendResponse".
 **/
Login.sendResponse = Plugin.responseMessage
Login.logout = function() {
    /**
     * @purpose Send a request to log out the user.
     **/
    Login.jumpToTab('login')

    $.post(Plugin.loginURL, { logout: true, loginURL: Plugin.loginURL }, (res) => {
        Login.sendResponse('You are now logged out.', 'INFO')

        if (typeof Login.onLogout === 'function') Login.onLogout()
    })
}

// Running initial logic for the Login module.
Plugin.start()
})()
