/**
 * @title Narrow Node Login Portal
 * @description A login interface that connects to a login server and stores cookies in the user's browser.
 * @author ethancrist
 **/

// [DEPENDENCIES]
const url = require('url')
const request = require('request')
const session = require('express-session')
const svgCaptcha = require('svg-captcha')
const apiUrl = 'http://localhost:3000'
const usersUrl = apiUrl + '/users'
const authUrl = usersUrl + '/auth'
const webcore = require('narrownode-webcore')

// [FUNCTIONS]
const isEmailAddress = function(input) {
    /**
     * @purpose Check if a string is an email address.
     **/
    return /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/.test(input)
}

// [ROUTES]
const login = function(req, res) {
    /**
     * @purpose Set a cookie for a successfully validated user.
     **/
    const username = req.body.login.username

    // Setting the cookie.
    req.session.username = username

    res.send({ status: 'SUCCESS', message: 'Welcome, '+username+'.' })
}
const logout = function(req, res, next) {
    /**
     * @purpose Log out the user by destroying their cookie.
     **/
    req.session.destroy()
    res.redirect('/')
}
const register = function(req, res, next) {
    /**
     * @purpose Create a new account.
     **/

    // Making sure the email is an email and the username doesn't include ' ' or '/'.
    if (!isEmailAddress(req.body.register.email)) return res.send({ status: 'ERROR', message: 'Invalid email.' })
    if (req.body.register.username.match(/ |\//g)) return res.send({ status: 'ERROR', message: 'Invalid username.' })
        return res.send(req.body.register.captcha+', '+JSON.stringify(req.session))

    if (req.body.register.captcha !== req.session.captcha) return res.send({ status: 'ERROR', message: 'CAPTCHA incorrect.' })

    // Made it this far; captcha was passed; removing it from the new user object...
    delete req.body.register.captcha

    const options = {
        url: usersUrl,
        headers: { 'Authorization': process.env.USERS_API_KEY },
        json: req.body.register
    }

    // Users API call
    request.post(options, (apiErr, apiRes, apiBody) => {
        // status options: SUCCESS, INFO, WARNING, ERROR
        apiRes.statusCode === 201 ? res.send({ status: 'INFO', message: 'Account created.' }) :
        apiRes.statusCode === 302 ? res.send({ status: 'WARNING', message: 'Username taken.' }) :
                                    res.send({ status: 'ERROR', message: 'Could not create user.' })
    })
}
const loadLoginModule = function(req, res, next) {
    /**
     * @purpose Send a script to load the Login module for the first time.
     **/
    const model = { loginURL: url.parse(req.originalUrl).pathname }

    res.set('Content-Type', 'application/javascript')
    return res.render(__dirname+'/views/load', model)
}
const handleLoginRequest = function(req, res, next) {
    /**
     * @purpose User requested to login; handle a response.
     **/
    const invalidFormats = [ undefined, null, "" ]
    if (invalidFormats.indexOf(req.body.login.username) > -1 || invalidFormats.indexOf(req.body.login.username) > -1)
        return res.status(400).send({ status: 'ERROR', message: 'Invalid format' })

    const options = {
        url: authUrl,
        headers: { 'Authorization': process.env.USERS_API_KEY },
        json: req.body.login
    }

    // Users API call
    request.post(options, (apiErr, apiRes, apiBody) => {
        // status options: SUCCESS, INFO, WARNING, ERROR
        !apiErr && apiBody.authorized ? login(req, res)
                                      : res.send({ status: 'ERROR', message: 'Invalid username or password.' })
    })
}

const listen = function(req, res, next) {
    if (req.query.load) return loadLoginModule(req, res, next)
    if (req.body.login) return handleLoginRequest(req, res, next)
    if (req.body.logout) return logout(req, res, next)
    if (req.body.register) return register(req, res, next)

    // Made it this far; serving the login page if overlay === true.
    const captcha = svgCaptcha.create()

    // Storing the captcha answer in the user's cookie.
    req.session.captcha = captcha.text

    // Sending the captcha HTML <svg.../svg> to the model.
    const model = { captcha: captcha.data, loginURL: req.body.loginURL }

    // If { overlay: true } was sent, rendering the Login module, otherwise move on.
    req.body.overlay ? res.render(__dirname+'/views/login', model) : next()
}

module.exports = function(options) {
    const defaultOptions = {
        url: '/login'
    } 
    if (!options.app) throw new Error("You must pass through your express app that you are using. require('narrownode-login-plugin')({ app: require('express')() })")
    if (!options.url) options.url = defaultOptions.url
    if (options.url[0] !== '/') options.url = '/' + options.url

    // Implementing cookies middleware. Each cookie will expire an hour after creation.
    options.app.use(session({ secret: process.env.COOKIE_SECRET, cookie: { secure: true, maxAge: 3600000 } }))

    // Listening on login endpoint of implementation's app.
    options.app.use(options.url, listen)

    // Adding Webcore.
    options.app.use(webcore)

}
